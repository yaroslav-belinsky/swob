angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($scope, $ionicModal, $timeout, $http, $ionicLoading, $interval, $rootScope) {
        // Form data for the login modal
        $scope.loginData = {};
        $scope.loginData = {
            username: 'Yaroslav',
            password: 'nokia6230I'
        };

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });
        $scope.doLogout = function () {
            $ionicLoading.show({
                template: 'Загрузка...'
            });
            var req = {
                method: 'GET',
                url: 'http://swob.mairon-studio.ru/api/logout'
            }
            $http(req).success(function (data, status, headers, config) {
                location.reload();
            }).error(function (err) {
                console.log(err);
            });
        };
        // Triggered in the login modal to close it
        $scope.closeLogin = function () {
            $scope.modal.hide();
        };

        $rootScope.getChatsOnModaretion = function () {
            $http({
                method: 'GET',
                url: 'http://swob.mairon-studio.ru/api/chatonmodaration'
            }).success(function (data, status, headers, config) {
                $scope.chatsModarate = data;
                $scope.chatsModarateCount = data.length;
                $ionicLoading.hide();
                console.log(data);
            }).error(function (err) {
                console.log(err);
            });
        };
        $rootScope.getChatsOnModaretion();
        $rootScope.getChats = function () {
            $http({
                method: 'GET',
                url: 'http://swob.mairon-studio.ru/api/chat'
            }).success(function (data, status, headers, config) {
                $scope.chats = data;
                $ionicLoading.hide();
                console.log(data);
            }).error(function (err) {
                console.log(err);
            });
        }
        $rootScope.getChats();
        $interval($rootScope.getChats, 360000);
        $interval($rootScope.getChatsOnModaretion, 360000);
        // Perform the login action when the user submits the login form
        $scope.doLogin = function () {
            $ionicLoading.show({
                template: 'Вход...'
            });
            $scope.modal.hide();
            var req = {
                method: 'POST',
                url: 'http://swob.mairon-studio.ru/api/login',
                /*headers: {
                 'Content-Type': undefined
                 },*/
                data: $scope.loginData
            }
            $http(req).success(function (data, status, headers, config) {
                if (!data.loggedin) {
                    console.log(data);
                    console.log(config);
                } else {
                    console.log(data);
                    console.log(config);

                    location.reload();
                }

            }).error(function (err) {
                console.log(err);
            });
        };
        $http({
            method: 'GET',
            url: 'http://swob.mairon-studio.ru/api/auth'
        }).success(function (data, status, headers, config) {
            if (data.status == 200) {
                $scope.loggedin = true;
                $scope.adminStatus = data.adminStatus;
            } else {
                $scope.loggedin = false;
                $scope.modal.show();
            }
            console.log(data);
        }).error(function (err) {
            console.log(err);
        });


    })

    .controller('ChatModareteCtrl', function ($scope, $http, $interval, $ionicLoading, $rootScope) {
        $ionicLoading.show({
            template: 'Загрузка...'
        });
        $rootScope.getChatsOnModaretion();
        $scope.addPost = function (id_sms) {
            $http({
                method: 'POST',
                url: 'http://swob.mairon-studio.ru/api/chat/' + id_sms + '/add'
            }).success(function (data, status, headers, config) {
                console.log(data);
                $rootScope.getChats();
                $rootScope.getChatsOnModaretion();
            }).error(function (err) {
                console.log(err);
            });
        };
        $scope.deletePost = function (id_sms) {
            $http({
                method: 'POST',
                url: 'http://swob.mairon-studio.ru/api/chat/' + id_sms + '/delete'
            }).success(function (data, status, headers, config) {
                console.log(data);
                $rootScope.getChats();
                $rootScope.getChatsOnModaretion();
            }).error(function (err) {
                console.log(err);
            });
        };
    })
    .controller('ChatCtrl', function ($scope, $http, $ionicLoading, $ionicPopup, $rootScope) {

        $scope.sendMessage = function (message) {
            $ionicLoading.show({
                template: 'Загрузка...'
            });
            $http({
                method: 'POST',
                url: 'http://swob.mairon-studio.ru/api/chat',
                data: {message:message}
            }).success(function (data, status, headers, config) {
                console.log(data);
                $scope.input.message = '';
                getChats();
                $ionicLoading.hide();
                    var alertPopup = $ionicPopup.alert({
                        title: 'Внимание!',
                        template: 'Ваше сообщение отправлено и принято на модерацию.'
                    });
                    alertPopup.then(function(res) {
                        console.log('Thank you for not eating my delicious ice cream cone');
                    });
            }).error(function (err) {
                console.log(err);
            });
        }
        $ionicLoading.show({
            template: 'Загрузка...'
        });
        $rootScope.getChats();



    });
